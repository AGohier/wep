import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/pages/HelloWorld'
import ParamSelect from '@/pages/ParamSelect'
import Login from '@/pages/Login'
import store from '@/store/index'

Vue.use(Router)

const rerouteIfNotLogged = (to, from, next) => {
  if (store.getters.isLoggedIn) {
    next()
    return
  }
  store.commit('setActiveTab', 'Login')
  next('/Login')
}

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },
    {
      path: '/ParamSelect',
      name: 'ParamSelect',
      component: ParamSelect,
      beforeEnter: rerouteIfNotLogged
    },
    {
      path: '/Login',
      name: 'Login',
      component: Login
    }
  ]
})
