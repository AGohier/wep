
export const state = {
  debug: true,
  activeUser: 'Aucun',
  activeTab: 'HelloWorld',
  userIsLogged: false,
  authToken: ''
}

export const mutations = {
  setActiveUser (state, newValue) {
    if (this.debug) { console.log('setActiveUserAction triggered with', newValue) }
    state.activeUser = newValue
  },
  clearActiveUser (state) {
    if (this.debug) { console.log('clearActiveUserAction triggered') }
    state.activeUser = 'Aucun'
  },
  setActiveTab (state, newValue) {
    if (this.debug) { console.log('setActiveTab triggered with', newValue) }
    state.activeTab = newValue
  },
  logingIn (state) {
    state.userIsLogged = true
  },
  logingOut (state) {
    state.userIsLogged = false
  },
  setAuthToken (state, newToken) {
    state.authToken = 'Bearer ' + newToken
  },
  resetAuthToken (state) {
    state.authToken = ''
  }
}

export const getters = {
  getActiveUser: state => {
    return state.activeUser
  },
  getActiveTab: state => {
    return state.activeTab
  },
  isLoggedIn: state => {
    return state.userIsLogged
  },
  getAuthToken: state => {
    return state.authToken
  }
}

export const actions = {}

export default {
  state,
  mutations,
  getters,
  actions
}
