import Vue from 'vue'
import Vuex from 'vuex'
import {state,
  mutations,
  getters,
  actions} from './store'

Vue.use(Vuex)

const store = new Vuex.Store({
  state,
  mutations,
  getters,
  actions
})

export default store
