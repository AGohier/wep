import Api from '@/services/Api'

export default {
  registerUser (identity) {
    return Api().post('users/register', identity).catch(function (error) { if (error.response) { return 'failed' } })
  },
  loginUser (identity) {
    return Api().post('users/login', identity).catch(function (error) { if (error.response) { return 'failed' } })
  },
  fetchSports (auth) {
    return Api().get('sports', {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  fetchSelectedSports (auth, id) {
    return Api().get('users/' + id + '/sports', {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  fetchLocations (auth) {
    return Api().get('locations', {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  fetchSelectedLocations (auth, id) {
    return Api().get('users/' + id + '/locations', {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  addSportToUserList (auth, id, sportId) {
    return Api().post('users/' + id + '/sports/' + sportId, {}, {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  addLocationToUserList (auth, id, locationId) {
    return Api().post('users/' + id + '/locations/' + locationId, {}, {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  removeSportToUserList (auth, id, sportId) {
    return Api().delete('users/' + id + '/sports/' + sportId, {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  },
  removeLocationToUserList (auth, id, locationId) {
    return Api().delete('users/' + id + '/locations/' + locationId, {'headers': {'Authorization': auth}}).catch(function (error) { if (error.response) { return 'failed' } })
  }
}
