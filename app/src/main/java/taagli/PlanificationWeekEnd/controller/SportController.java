package taagli.PlanificationWeekEnd.controller;

import taagli.PlanificationWeekEnd.domain.Sport;
import taagli.PlanificationWeekEnd.dto.SportDto;
import taagli.PlanificationWeekEnd.service.SportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping(value = "/sports")
public class SportController {
    @Autowired
    SportService sportService;

    @GetMapping(value = "/{id}")
    public ResponseEntity<SportDto> getSport(@PathVariable Long id){

        return new ResponseEntity<>(sportService.getOne(id),HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<Set<SportDto>> getSports(){

        return new ResponseEntity<>(sportService.getAll(),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> saveSport(@RequestBody Sport sport){


        return new ResponseEntity<>(sportService.save(sport),HttpStatus.OK);
    }
    @RequestMapping(value = "/{id}",method = RequestMethod.DELETE)
    public ResponseEntity<String> deleteSport(@PathVariable Long id){

        return new ResponseEntity<>(sportService.delete(id),HttpStatus.OK);
    }
}
