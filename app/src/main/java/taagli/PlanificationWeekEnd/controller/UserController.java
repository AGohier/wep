package taagli.PlanificationWeekEnd.controller;

import taagli.PlanificationWeekEnd.domain.LoginResponse;
import taagli.PlanificationWeekEnd.domain.User;
import taagli.PlanificationWeekEnd.dto.LocationDto;
import taagli.PlanificationWeekEnd.dto.SportDto;
import taagli.PlanificationWeekEnd.dto.UserDto;
import taagli.PlanificationWeekEnd.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;


@RestController()
@RequestMapping(value = "/users")
public class UserController {


    private final UserService userService;

    private final ModelMapper modelMapper;

    public UserController(UserService userService, ModelMapper modelMapper) {
        this.userService = userService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public ResponseEntity<Set<UserDto>> getAllUsers(){
        return new ResponseEntity<>(userService.getAllDto(),HttpStatus.OK);
    }

    @GetMapping("/me")
    public ResponseEntity<UserDto> whoami(HttpServletRequest req){
        return new ResponseEntity<>(userService.whoami(req), HttpStatus.OK);
    }

    @GetMapping("/{idUser}/sports")
    public ResponseEntity<Set<SportDto>> getUserSports(@PathVariable("idUser") Long id){
        return new ResponseEntity<>(userService.getSports(id),HttpStatus.OK);
    }

    @GetMapping("/{idUser}/locations")
    public ResponseEntity<Set<LocationDto>> getUserLocations(@PathVariable("idUser") Long id){
        return new ResponseEntity<>(userService.getLocations(id),HttpStatus.OK);
    }

    @PostMapping("/login")
    @ApiOperation(value = "${UserController.login}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"), //
            @ApiResponse(code = 422, message = "Invalid username/password supplied")})
    public ResponseEntity<LoginResponse> login(@ApiParam("login User") @RequestBody UserDto userDto) {
        User user = modelMapper.map(userDto,User.class);
        return new ResponseEntity<>(userService.signin(user.getUsername(), user.getPassword()), HttpStatus.OK);
    }

    @PostMapping("/register")
    @ApiOperation(value = "${UserController.register}")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Something went wrong"),
            @ApiResponse(code = 403, message = "Access denied"), //
            @ApiResponse(code = 422, message = "Username is already in use"),
            @ApiResponse(code = 500, message = "Expired or invalid JWT token")})
    public String register(@ApiParam("register User") @RequestBody UserDto userDto) {
        User user = modelMapper.map(userDto,User.class);
        userService.signup(user);
        return "Register ok";
    }

    @PostMapping(value = "/{idUser}/sports/{idSport}")
    public ResponseEntity<String> addUserSport(@PathVariable("idUser") Long idUser, @PathVariable("idSport") Long idSport){
        userService.addUserSport(idUser,idSport);
        return new ResponseEntity<>("{\"message\":\"Sports has been successfully added to user\"}", HttpStatus.OK);
    }


    @PostMapping(value = "/{idUser}/locations/{idLocation}")
    public ResponseEntity<String> getUserLocation(@PathVariable("idUser") Long idUser, @PathVariable("idLocation") Long idLocation){
        userService.addUserLocation(idUser,idLocation);
        return new ResponseEntity<>("{\"message\":\"Location has been successfully added to user\"}",HttpStatus.OK);
    }

    @DeleteMapping(value = "/{idUser}")
    public ResponseEntity<String> deleteUser(@PathVariable("idUser") Long id){
        return new ResponseEntity<>(userService.delete(id),HttpStatus.OK);
    }

    @DeleteMapping(value = "/{idUser}/sports/{idSport}")
    public void deleteUserSport(@PathVariable("idUser") Long idUser , @PathVariable("idSport") Long idSport){
        userService.deleteSport(idUser, idSport);
    }

    @DeleteMapping(value = "/{idUser}/locations/{idLocation}")
    public void deleteUserLocation(@PathVariable("idUser") Long idUser , @PathVariable("idLocation") Long idLocation){
        userService.deleteLocation(idUser, idLocation);
    }
}
