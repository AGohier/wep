package taagli.PlanificationWeekEnd.controller;

import taagli.PlanificationWeekEnd.domain.Location;
import taagli.PlanificationWeekEnd.dto.LocationDto;
import taagli.PlanificationWeekEnd.service.LocationService;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(value = "/locations")
public class LocationController {


    private final LocationService locationService;
    private final ModelMapper modelMapper;

    public LocationController(LocationService locationService, ModelMapper modelMapper){
        this.locationService = locationService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public ResponseEntity<Set<LocationDto>> getAllLocations(){
        return new ResponseEntity<>(convertToDto(locationService.getAll()),HttpStatus.OK);
    }
    @GetMapping(value = "/{id}")
    public ResponseEntity<Location> getLieu(@PathVariable Long id){
        return new ResponseEntity<>(locationService.getOne(id),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<String> saveLieu(@RequestBody Location location){
        return new ResponseEntity<>(locationService.save(location),HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteLieu(@PathVariable Long id){
        return new ResponseEntity<>(locationService.delete(id), HttpStatus.OK);
    }

    private Set<LocationDto> convertToDto(Set<Location> locations){
        Set<LocationDto> locationsDto = new HashSet<>();
        locations.forEach(location -> locationsDto.add(modelMapper.map(location,LocationDto.class)));
        return locationsDto;
    }

}
