package taagli.PlanificationWeekEnd;

import taagli.PlanificationWeekEnd.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PlanificationWeekEndApplication {

	@Autowired
	UserService userService;

	public static void main(String[] args) {
		SpringApplication.run(PlanificationWeekEndApplication.class, args);
	}

	/*@Override
	public void run(String... params) throws Exception {
		User admin = new User();
		admin.setUsername("admin");
		admin.setPassword("admin");
		admin.setEmail("admin@email.com");
		admin.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_ADMIN)));

		userService.signup(admin);

		User client = new User();
		client.setUsername("client");
		client.setPassword("client");
		client.setEmail("client@email.com");
		client.setRoles(new ArrayList<Role>(Arrays.asList(Role.ROLE_CLIENT)));

		userService.signup(client);

		User p1 = new User("toto","titi","donge.maxime@gmail.com");
		//Sport tennis = new Sport("tennis");
		Sport surf = new Sport("surf", "Rain");
		Sport paddle = new Sport("paddle");
		Sport foot = new Sport("foot");
		Sport golf = new Sport("golf");
		userService.save(p1, new HashSet<>(Arrays.asList(surf,foot,paddle,golf)));

	}*/
}
