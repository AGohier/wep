package taagli.PlanificationWeekEnd.repository;

import taagli.PlanificationWeekEnd.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface UserRepository extends CrudRepository<User,Long> {



    @Override
    Set<User> findAll();

    @Query("select u from User u join fetch u.locations join fetch u.sports")
    Set<User> findAllWithSportsAndUsers();


    boolean existsByUsername(String username);

    User findByUsername(String username);
}
