package taagli.PlanificationWeekEnd.repository;

import taagli.PlanificationWeekEnd.domain.Sport;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface SportRepository extends CrudRepository<Sport,Long> {

    @Override
    Set<Sport> findAll();
}
