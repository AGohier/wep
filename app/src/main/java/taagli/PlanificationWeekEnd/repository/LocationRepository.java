package taagli.PlanificationWeekEnd.repository;

import taagli.PlanificationWeekEnd.domain.Location;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface LocationRepository extends CrudRepository<Location,Long> {

    @Override
    Set<Location> findAll();

}
