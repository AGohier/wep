package taagli.PlanificationWeekEnd.util;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * Created by donge on 09/11/2018.
 */
public class SmtpAuthenticator extends Authenticator {
    private final String username;
    private final String password;

    public SmtpAuthenticator(String username, String password) {
        super();
        this.username = username;
        this.password = password;

    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        String username = this.username;
        String password = this.password;
        if ((username != null) && (username.length() > 0) && (password != null)
                && (password.length() > 0)) {

            return new PasswordAuthentication(username, password);
        }

            return null;
        }
}

