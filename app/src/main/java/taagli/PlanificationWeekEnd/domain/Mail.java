package taagli.PlanificationWeekEnd.domain;

import org.springframework.stereotype.Component;


public class Mail {

    private String username ;
    private String password ;

    private String fromMail ;
    private String toMail ;
    private String subject ;
    private String text ;

    public Mail (String text, String toMail) {
        this.username = "weplanner.d.g@gmail.com";
        this.password = "maxime.arnaud";
        this.fromMail = "weplanner.d.g@gmail.com";
        this.toMail = toMail;
        this.subject = "Week End Planner: Rapport de la semaine";
        this.text = text;
    }
    public Mail(String _username, String _password, String _fromMail ) {
        username = _username;
        password = _password;
        fromMail = _fromMail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFromMail() {
        return fromMail;
    }

    public void setFromMail(String fromMail) {
        this.fromMail = fromMail;
    }

    public String getToMail() {
        return toMail;
    }

    public void setToMail(String toMail) {
        this.toMail = toMail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {

        this.text = text;
    }
}


