package taagli.PlanificationWeekEnd.domain;

import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@BatchSize(size = 10)
public class User {

    private Long id;

    private String username;
    private String email;
    private String password;
    private Set<Sport> sports;
    private Set<Location> locations;
    private List<Role> roles;


    public User(){
    }

    public User(String username, String password, String email, Set<Sport> sports) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.sports = sports;
    }

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.sports = new HashSet<>();
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_sport",
            joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "sport_id", referencedColumnName = "id")})

    public Set<Sport> getSports() {
        return sports;
    }
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "user_location",
            joinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "location_id", referencedColumnName = "id")}
    )
    public Set<Location> getLocations() {
        return locations;
    }

    public void setLocations(Set<Location> locations) {
        this.locations = locations;
    }

    public void setSports(Set<Sport> sports) {
        this.sports = sports;
    }

    public void addSport(Sport sport){
        this.sports.add(sport);
    }

    public void deleteSport(Long idSport){
        this.sports.removeIf(sport -> sport.getId() == idSport);
    }

    public void addLocation(Location location){
        this.locations.add(location);
    }

    public void deleteLocation(Long idLocation){
        this.locations.removeIf(location -> location.getId() == idLocation);
    }

}
