package taagli.PlanificationWeekEnd.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Sport {

    private Long id;

    private String label;

    private String optimalWeather;

    private Set<User> users;

    public Sport(){
    }

    public Sport(String libelle) {
        this.label = libelle;
        this.users = new HashSet<>();
        this.optimalWeather = "Clear";
    }

    public Sport(String libelle, String optimalWeather) {
        this.label = libelle;
        this.users = new HashSet<>();
        this.optimalWeather = optimalWeather;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String libelle) {
        this.label = libelle;
    }
    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },mappedBy = "sports")
    public Set<User> getPersonne() {
        return users;
    }

    public void setPersonne(Set<User> users) {
        this.users = users;
    }
    @Column(name = "optimalweather")
    public String getOptimalWeather() {
        return optimalWeather;
    }

    public void setOptimalWeather(String optimalWeather) {
        this.optimalWeather = optimalWeather;
    }
}
