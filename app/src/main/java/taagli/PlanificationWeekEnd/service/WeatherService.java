package taagli.PlanificationWeekEnd.service;

import java.net.URI;
import java.text.MessageFormat;
import java.util.Set;

import taagli.PlanificationWeekEnd.configuration.PlanificationWeekEndProperties;
import taagli.PlanificationWeekEnd.domain.Location;
import taagli.PlanificationWeekEnd.domain.Mail;
import taagli.PlanificationWeekEnd.domain.User;
import taagli.PlanificationWeekEnd.domain.Sport;
import taagli.PlanificationWeekEnd.domain.owm.Weather;
import taagli.PlanificationWeekEnd.domain.owm.WeatherEntry;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;


@Service
public class WeatherService {

    private final RestTemplate restTemplate;
    private final UserService userService;
    private final MailService mailService;

    private final String apiKey;
    private final String apiUrl;



    public WeatherService(RestTemplateBuilder restTemplateBuilder, PlanificationWeekEndProperties properties,
                          SportService sportService, UserService userService, MailService mailService){
        this.apiKey = properties.getApi().getKey();
        this.apiUrl = properties.getApi().getUrl();
        this.restTemplate = restTemplateBuilder.build();
        this.userService = userService;
        this.mailService = mailService;
    }
   // @Scheduled(cron = )
    public Weather getForecast(String city){
        URI url = new UriTemplate(apiUrl).expand(city, apiKey);
        RequestEntity<?> request = RequestEntity.get(url)
                .accept(MediaType.APPLICATION_JSON).build();
        ResponseEntity<Weather> exchange = this.restTemplate
                .exchange(request, Weather.class);
       return exchange.getBody();
    }
    @Scheduled(fixedDelay = 30000)
    private void evalutate() throws Exception {

        Set<User> users = userService.getAll();
        for (User p : users){
           Set<Sport> sports = p.getSports();
           Set<Location> lieuxes = p.getLocations();
           Location bestLocation = new Location();
           for (Location l : lieuxes){
               double note = 0;
                Weather aWeather = this.getForecast(l.getname());
                for (Sport s : sports){
                    // todo recup bonne condition pour le sport ?
                    //aWeather.getEntries().forEach(w -> System.out.println("for each weather "+ w.getWeatherType()+ " date : "+w.getDate()));
                    WeatherEntry weatherEntry = aWeather.getEntries().get(10);
                    System.out.println("WeatherType = "+ weatherEntry.getWeatherType());
                    System.out.println("optimal weather for  "+ s.getLabel() + " " +s.getOptimalWeather());
                    if (weatherEntry.getWeatherType().equals(s.getOptimalWeather())){
                        note += 1;
                    }
                    if (weatherEntry.getWeatherType().equals("Clouds")){
                        note += 0.5;
                    }
                    if (weatherEntry.getTemperature() >= 15){
                        note += 2;
                    }
                    System.out.println(" temperature = "+ weatherEntry.getTemperature());
                    if (weatherEntry.getTemperature() >= 10 && weatherEntry.getTemperature() < 15){
                        note += 1;
                    }
                }
                l.setNote(note);
               if (bestLocation.getNote() < l.getNote() || bestLocation.getNote() == 0){
                    bestLocation = l;
               }
               System.out.println("lieu : " +l.getname() + " note : "+ l.getNote());
           }
            String templateText = "Hi {0}, \n\nOur system has analysed the best location for sports you have set on week-end-planificator.com.\n " +
                    "It seems that {1} is the best location" + " for practising your favorites sports next week end." +
                    "\n\n\nWe wish you a pleasant week end.\n\n" +
                    "Regards,\n" +
                    "Week End Planificator Team.";
            String msg = MessageFormat.format(templateText, p.getUsername(),bestLocation.getname());
            mailService.send(new Mail(msg,p.getEmail()));
        }
    }
}
