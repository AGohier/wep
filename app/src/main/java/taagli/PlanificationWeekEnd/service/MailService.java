package taagli.PlanificationWeekEnd.service;

import taagli.PlanificationWeekEnd.domain.Mail;
import taagli.PlanificationWeekEnd.util.SmtpAuthenticator;
import org.springframework.stereotype.Service;

import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

@Service
public class MailService {
    public void send(Mail mail) {

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        SmtpAuthenticator authenticator = new SmtpAuthenticator (mail.getUsername(), mail.getPassword());

        Session session = Session.getInstance(props, authenticator );

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mail.getFromMail()));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(mail.getToMail()));
            message.setSubject(mail.getSubject());
            message.setText(mail.getText());

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}