package taagli.PlanificationWeekEnd.service;

import taagli.PlanificationWeekEnd.domain.Location;
import taagli.PlanificationWeekEnd.domain.LoginResponse;
import taagli.PlanificationWeekEnd.domain.User;
import taagli.PlanificationWeekEnd.domain.Sport;
import taagli.PlanificationWeekEnd.dto.LocationDto;
import taagli.PlanificationWeekEnd.dto.SportDto;
import taagli.PlanificationWeekEnd.dto.UserDto;
import taagli.PlanificationWeekEnd.exception.WeekEndPlanifierException;
import taagli.PlanificationWeekEnd.repository.UserRepository;
import taagli.PlanificationWeekEnd.security.JwtTokenProvider;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Service
public class UserService {


    private final UserRepository userRepository;
    private final SportService sportService;
    private final LocationService locationService;
    private final PasswordEncoder passwordEncoder;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final ModelMapper modelMapper;

    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider,
                       AuthenticationManager authenticationManager, ModelMapper modelMapper, SportService sportService, LocationService locationService){
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
        this.modelMapper = modelMapper;
        this.sportService = sportService;
        this.locationService = locationService;

    }

    public LoginResponse signin(String username, String password){
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username,password));
            User user = userRepository.findByUsername(username);
            String token = jwtTokenProvider.createToken(user.getUsername(),user.getRoles());
            return new LoginResponse(user.getId(),token);
        } catch (AuthenticationException e){
            throw new WeekEndPlanifierException("Username / password is incorrect", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void signup(User user){
        if (!userRepository.existsByUsername(user.getUsername())){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            userRepository.save(user);
        } else {
            throw new WeekEndPlanifierException("Username already exists", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public Set<SportDto> getSports(long id){
        User user = userRepository.findById(id).orElse(null);
        Set<SportDto> sportsDto = new HashSet<>();
        if (user != null){
            Set<Sport> sports =  user.getSports();
            sports.forEach(sport -> {
                sportsDto.add (modelMapper.map(sport,SportDto.class));
            });
            return sportsDto;
        }
        else {
            throw new WeekEndPlanifierException("User does not exists", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public Set<LocationDto> getLocations(Long id){
        User user = userRepository.findById(id).orElse(null);
        Set<LocationDto> locationsDtos = new HashSet<>();
        if (user != null){
            Set<Location> sports =  user.getLocations();
            sports.forEach(location -> {
                locationsDtos.add (modelMapper.map(location,LocationDto.class));
            });
            return locationsDtos;
        }
        else {
            throw new WeekEndPlanifierException("User does not exists", HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void save(User user, Set<Sport> sports) {
        Set<Sport> hashSetSports = new HashSet<>();
        Set<User> hashSetUsers = new HashSet<>();
        hashSetUsers.add(user);
        Location paimpol = new Location("paimpol");
        paimpol.setUsers(hashSetUsers);
        for (Sport sport : sports) {
            hashSetSports.add(sport);
            sport.setPersonne(hashSetUsers);
        }
        user.setLocations(new HashSet<>(Arrays.asList(paimpol)));
        user.setSports(hashSetSports);

        userRepository.save(user);
    }

    public void addUserSport(Long idUser, Long idSport){
        User user;
        Sport sport;
        if (userRepository.existsById(idUser)){
            if (sportService.exists(idSport)){
                user = get(idUser);
                sport = modelMapper.map(sportService.getOne(idSport),Sport.class);
                user.addSport(sport);
                if (sport.getOptimalWeather() == null){
                    sport.setOptimalWeather("Clear");
                }
                userRepository.save(user);
            }
        }
        else if (!userRepository.existsById(idUser) || !sportService.exists(idSport)){
            throw new WeekEndPlanifierException("cannot find sport with id :"+ idSport + "and/or user with id "+ idUser, HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }

    public void addUserLocation(Long idUser, Long idLocation){
        User user;
        Location location;
        if (userRepository.existsById(idUser)){
            if (locationService.exists(idLocation)){
                user = get(idUser);
                location = locationService.getOne(idLocation);
                user.addLocation(location);
                userRepository.save(user);
            }
        }
        else if (!userRepository.existsById(idUser) || !locationService.exists(idLocation)){
            throw new WeekEndPlanifierException("cannot find location with id :"+ idLocation + "and/or user with id "+ idUser, HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }

    public UserDto whoami(HttpServletRequest req) {
        User user = userRepository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(req)));
        return modelMapper.map(user, UserDto.class);
    }

    public Set<User> getAll() {
        return userRepository.findAllWithSportsAndUsers();
    }

    public Set<UserDto> getAllDto() {
        Set<UserDto> usersDto = new HashSet<>();
        userRepository.findAll().forEach(user -> {
            usersDto.add(modelMapper.map(user,UserDto.class));
        });
        return usersDto;
    }

    public User get(Long id){
        return userRepository.findById(id).get();
    }

    public String delete(Long id){
        if (userRepository.existsById(id)){
            userRepository.deleteById(id);
            return "User with id : " + id  +"has been deleted";
        }
        else {
            throw new WeekEndPlanifierException("Cannot delete user with id : " + id + " because no entity found with this id", HttpStatus.UNPROCESSABLE_ENTITY);
        }

    }

    public void deleteSport(Long idUser, Long idSport){
        User user = null;
        if (userRepository.existsById(idUser)){
            user = get(idUser);
            if (sportService.exists(idSport)){
                user.deleteSport(idSport);
                userRepository.save(user);
            }
        }
        else {
            throw new WeekEndPlanifierException("sport with id :"+ idSport+ "does not exists or is no longer linked with user : "+ user.getUsername() , HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    public void deleteLocation(Long idUser, Long idLocation){
        User user = null;
        if (userRepository.existsById(idUser)){
            user = get(idUser);
            if (locationService.exists(idLocation)){
                user.deleteLocation(idLocation);
                userRepository.save(user);
            }
        }
        else {
            throw new WeekEndPlanifierException("location with id :"+ idLocation+ "does not exists or is no longer linked with user : "+ user.getUsername() , HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

}
