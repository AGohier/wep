package taagli.PlanificationWeekEnd.service;

import taagli.PlanificationWeekEnd.domain.Location;
import taagli.PlanificationWeekEnd.exception.WeekEndPlanifierException;
import taagli.PlanificationWeekEnd.repository.LocationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class LocationService {


    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public boolean exists(Long id){
        return locationRepository.existsById(id);
    }

    public Set<Location> getAll(){
        return locationRepository.findAll();
    }

    public Location getOne(Long id){
        return locationRepository.findById(id).get();
    }

    public String save(Location location){
        locationRepository.save(location);
        return "Location "+ location.getname() + " has been save";
    }

    public String delete(Long id){
        if (exists(id)){
            locationRepository.deleteById(id);
            return "Location with id  " + id + " has been deleted";
        }
        else {
            throw new WeekEndPlanifierException("Unable to delete location with id "+ id, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }
}
