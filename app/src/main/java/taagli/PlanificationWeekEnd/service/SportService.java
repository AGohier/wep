package taagli.PlanificationWeekEnd.service;

import taagli.PlanificationWeekEnd.domain.Sport;
import taagli.PlanificationWeekEnd.dto.SportDto;
import taagli.PlanificationWeekEnd.repository.SportRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class SportService {


    public final SportRepository sportRepository;
    private final ModelMapper modelMapper;

    public SportService(SportRepository sportRepository, ModelMapper modelMapper){
        this.sportRepository = sportRepository;
        this.modelMapper = modelMapper;
    }

    public SportDto getOne(Long id){
        return modelMapper.map(sportRepository.findById(id).get(),SportDto.class);
    }

    public Set<SportDto> getAll(){
        Set<SportDto> sportsDto = new HashSet<>();
        sportRepository.findAll().forEach(sport -> {
            sportsDto.add(modelMapper.map(sport,SportDto.class));
        });
        return sportsDto;
    }

    public boolean exists(Long id){
        return sportRepository.existsById(id);
    }

    public String save(Sport sport) {
        if (sport.getOptimalWeather() == null){
            sport.setOptimalWeather("Clear");
        }
        sportRepository.save(sport);
        return "Sport "+ sport.getLabel()+" has been saved";
    }

    public String delete(Long id){
        sportRepository.deleteById(id);
        return "Sport has been deleted";
    }
}
