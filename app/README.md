# TAA Project Back-End
 This is Arnaud Gohier / Maxime Dongé repository for taa project  
 You can find below summary of project structure and instruction for test the project.

# Stack

![](https://img.shields.io/badge/java_8-✓-blue.svg)
![](https://img.shields.io/badge/spring_boot-✓-blue.svg)
![](https://img.shields.io/badge/swagger_2-✓-blue.svg)
![](https://img.shields.io/badge/mysql-✓-blue.svg)

# File structure

```
spring-boot-jwt/
 │
 ├── src/main/java/
 │   └── taagli.PlanificationWeekEnd
 │       ├── configuration
 │       │   └── AppConfig.java
 │       │   └── PlanificationWeekEndProperties.java
 │       │   └── SwaggerConfig.java
 │       │
 │       ├── controller
 │       │   └── LocationController.java
 │       │   └── SportController.java
 │       │   └── UserController.java
 │       │
 │       ├── domain
 │       │   ├── owm
 │       │       ├── Weather.java
 │       │       ├── WeatherEntry.java
 │       │   ├── Location.java
 │       │   └── LoginResponse.java
 │       │   └── Mail.java
 │       │   └── Role.java
 │       │   └── Sport.java
 │       │   └── User.java
 │       │
 │       ├── dto
 │       │   ├── LocationDto.java
 │       │   ├── SportDto.java
 │       │   └── UserDto.java
 │       │
 │       ├── exception
 │       │   ├── WeekEndPlanifierException.java
 │       │
 │       │
 │       ├── repository
 │       │   └── LocationRepository.java
 │       │   └── SportRepository.java
 │       │   └── UserRepository.java
 │       │
 │       ├── security
 │       │   ├── JwtTokenFilter.java
 │       │   ├── JwtTokenFilterConfigurer.java
 │       │   ├── JwtTokenProvider.java
 │       │   ├── CustomUserDetails.java
 │       │   └── WebSecurityConfig.java
 │       │
 │       ├── service
 │       │   └── LocationService.java
 │       │   └── MailService.java
 │       │   └── SportService.java
 │       │   └── UserService.java
 │       │   └── WeatherService.java
 │       │
 │       │
 │       ├── util
 │       │   └── SmtpAuthenticator.java
 |       |
 │       └── PlanificationWeekEndApplication.java
 │
 ├── src/main/resources/
 │   └── application.properties
 │
 ├── .gitignore
 ├── LICENSE
 ├── mvnw/mvnw.cmd
 ├── README.md
 └── pom.xml
 └── weplanifier.sql
```
# How to run the project ?

  You can run the project by cloning this repository (use master branch) and run it (build with maven or run project with intellij) locally on your computer.
  Before you must create the mysql database. you can import our test DB by using sql script weplanifier.sql (available in this repository).
  
  By default spring will try to connect to local mysql database named weplanifier, with username and password.
  
  The application.properties show you credential we used and mysql schema strategy : 
  ```application.properties
    spring.jpa.hibernate.ddl-auto=update
    spring.datasource.url=jdbc:mysql://localhost:3306/weplanifier
    spring.datasource.username=weplanifier
    spring.datasource.password=planif1234
  ```
  
  If you have trouble with mysql you can use H2 in memory database by uncommenting these lines in application.properties :
  ```application.properties
      spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_ON_EXIT=FALSE
      spring.h2.console.enabled=true
      spring.datasource.username=sa
      spring.datasource.password=
      spring.datasource.driverClassName=org.h2.Driver
      spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.H2Dialect
   ```
    
# Available functionnality

   This spring boot project use spring security and authentication with JWT token.
   If you want to try this API you must register and then login (/users/login or /users/register) and then, precise token in header  key :Authorization  value : Bearer {token}.
   
   About functionnality this API is able to : create new user, get/add/delete sports and locations of an user, create,get,delete location and sport.
   this application is able to reach openWeathen Api and then send email to users.
   For testing purpose, the @Scheduled for email sending is triggered every 30 seconds feels free to change it :
   ```
   //WeatherService
   @Scheduled(fixedDelay = 30000) -> milliseconds
       private void evalutate() throws Exception {
   
           Set<User> users = userService.getAll();
   ```
   Api's operations are described in Swagger ui : localhost:8080/swagger-ui.html
# Contact 

    mail : donge.maxime@gmail.com
  
  